FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
ARG SPRING_ACTIVE_PROFILE
MAINTAINER Nidio Dolfini
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install "-Dmaven.test.skip=true", "-Dspring.data.mongodb.uri=mongodb+srv://user-voting:L0Fl3e0h5FzYwTc5@voting.o51yl.mongodb.net/votingServiceDB?retryWrites=true&w=majority", "-Dspring.data.mongodb.database=votingServiceDB" && mvn package
FROM openjdk:11-slim
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/voting-api-*.jar /app/voting-api.jar
ENTRYPOINT ["java","-Dskip.unit-tests=true","-Dskip.integration-tests=true", "-Dskip.end-to-end-tests=true", "-Dspring.data.mongodb.uri=mongodb+srv://user-voting:L0Fl3e0h5FzYwTc5@voting.o51yl.mongodb.net/votingServiceDB?retryWrites=true&w=majority", "-Dspring.data.mongodb.database=votingServiceDB", "-jar", "voting-api.jar"]
